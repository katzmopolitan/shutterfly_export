// Called when the url of a tab changes.
function checkForValidUrl(tabId, changeInfo, tab) {
  if (
    tab.url.match(/.*.shutterfly.com\/(\d)+/) ||
    tab.url.match(/.*.shutterfly.com\/.+\/(\d)+/)
  ) {
    if (changeInfo.status === "complete") {
      chrome.tabs.executeScript(tabId, { file: "lib/albumDetailSnippet.js" });
      chrome.pageAction.show(tabId);
    }
  }
}

// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForValidUrl);

// Long-lived connections
console.log("runtime.onConnect.addListener");
chrome.runtime.onConnect.addListener(function(port) {
  console.log("Port name " + JSON.stringify(port));
  port.onMessage.addListener(function(message) {
    console.log("addListener: got message " + JSON.stringify(message));
    if (message.action === "check-license") {
      let key = message.key;
      let url = "https://still-journey-6188.herokuapp.com/license?key=" + key;
      jQuery
        .get(url)
        .done(function(data, status) {
          console.log(
            "chrome.runtime.onConnect.addListener: Request to license server completed"
          );
          port.postMessage({ data: data, status: status });
        })
        .fail(function(resp, status) {});
    } else {
      console.error(
        "chrome.runtime.onConnect.addListener: uknown message " +
          JSON.stringify(message)
      );
    }
  });
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  // setTimeout to simulate any callback (even from storage.sync)
  console.log(JSON.stringify(request));
  if (request.action === "getitems") {
    var site = request.data.site;
    var page = request.data.page;
    var nodeId = request.data.nodeId;
    var startIndex = request.data.startIndex;
    var url =
      "https://cmd.shutterfly.com/commands/pictures/getitems?site=" +
      site +
      "&amp;";

    // The url is https://[page].shutterfly.com/[nodeId]
    jQuery
      .post(url, {
        startIndex: startIndex,
        page: page,
        nodeId: nodeId
      })
      .done(function(data, status) {
        sendResponse({ data: data });
      })
      .fail(function(resp, status) {
        sendResponse({ responseText: resp.responseText });
      });
  }
  return true;
});
