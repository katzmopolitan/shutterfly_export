Download all pictures from your Shutterfly album to your computer

This simple extension allows you to download the contents of
your Shutterfly album to your computer for backup.

It can be added to Chrome via this [link](https://chrome.google.com/webstore/developer/dashboard/g11113841428504920720?authuser=0&page=1)

# Manual

To manually run the service

## Add dependecies

```
newScript = document.createElement('script');
newScript.type = 'text/javascript';
newScript.src = 'https://code.jquery.com/jquery-2.1.1.min.js';
document.getElementsByTagName('head')[0].appendChild(newScript);

newScript = document.createElement('script');
newScript.type = 'text/javascript';
newScript.src = 'https://rawgit.com/freethenation/durable-json-lint/master/lib/durable-json-lint-with-dependencies.js';
document.getElementsByTagName('head')[0].appendChild(newScript);

newScript = document.createElement('script');
newScript.type = 'text/javascript';
newScript.src = 'https://cdn.ravenjs.com/3.8.1/raven.min.js';
document.getElementsByTagName('head')[0].appendChild(newScript);
```

## New format

Examples:

```
var albumUrl="https://nahiii.shutterfly.com/pictures/60";
```

## Legacy format

Examples:

```
var albumUrl="https://theisles.shutterfly.com/27";
// Page with just one image
var albumUrl="https://ankitweddingpics.shutterfly.com/pictures/248";
var albumUrl="https://mgmillerphotography.shutterfly.com/lsu/415";
var albumUrl="https://cornerstonepedclassof2021.shutterfly.com/1stgrade/281";
```

## Unsupported albums

Example:

https://photos.shutterfly.com/album/20029261586

## Download pictures

```
var service = new ShutterflyBackupService(Shr.S.collectionKey, Shr.S.albumKey, Shr.S.siteName);
service.albumUrl=albumUrl;
service.debug = true;
service.downloadAlbum();
```

## Deployment

```
brew install node

npm install jsonfile

npm install shelljs

```

When ready to run deployment:

- Update `manifest.json` with the next version

- `./deploy.js` will produce zip file to upload

https://stackoverflow.com/questions/54126343/how-to-fix-unchecked-runtime-lasterror-the-message-port-closed-before-a-respon
