$(".go-personal").on("click", function() {
  checkLicense("free")
    .then(function(response) {})
    .catch(function(response) {});
  go();
});

$("#license-key").on("input", function() {
  let value = $(this).val(); // get the current value of the input field.
  saveLicenseKey(value);
});

$(".go-professional").on("click", function() {
  console.log("Professional use");
  let key = $("#license-key").val();
  checkLicense(key)
    .then(function(response) {
      go();
    })
    .catch(function(response) {
      $("#invalid-license").show();
    });
});

$("#personal").on("click", function() {
  $(".personal-dialog").show();
  $(".professional-dialog").hide();
});

$("#professional").on("click", function() {
  retrieveLicenseKey();
  $(".professional-dialog").show();
  $(".personal-dialog").hide();
});

$("#ack-personal").on("change", function() {
  if (this.checked) {
    $(".go-personal").prop("disabled", false);
  } else {
    $(".go-personal").prop("disabled", true);
  }
});

$("#ack-professional").on("change", function() {
  if (this.checked) {
    $(".go-professional").prop("disabled", false);
  } else {
    $(".go-professional").prop("disabled", true);
  }
});

function sendMessageToTab(message) {
  chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    // message = Object.assign(message, { url: tabs[0].url });
    // sendMessageToTab(message);
    chrome.tabs.sendMessage(
      tabs[0].id,
      {
        tab: tabs[0],
        message: message,
        url: tabs[0].url
      },
      function(response) {
        console.log(
          "sendMessageToTab: Got response " + JSON.stringify(response)
        );
      }
    );
  });
}

function sendPortMessage(messsage) {
  return new Promise(function(resolve, reject) {
    console.log("sendPortMessage: sending message " + JSON.stringify(messsage));
    var port = chrome.runtime.connect({ name: "LogLivedPort" });
    port.postMessage(messsage);
    port.onMessage.addListener(function(msg) {
      if (msg.status === "success") {
        console.log(
          "sendPortMessage: received response back " + JSON.stringify(msg)
        );
        resolve(msg);
      } else {
        console.log("sendPortMessage: recieved non-success status");
        reject(msg);
      }
    });
  });
}

function go() {
  console.log("User asked to download pictures. Ok then ...");
  var message = {
    action: "download",
    uniqueNames: $("#unique-name").attr("checked")
  };
  sendMessageToTab(message);
}

// Private

let LICENSE_KEY_NAME = "license";
let STORAGE_KEY_NAME = "shutterfly-dictionary";

function checkLicense(key) {
  console.log("Check license. Ok then ...");
  return new Promise(function(resolve, reject) {
    storeToStorage(LICENSE_KEY_NAME, key).then(function(dictionary) {
      console.log(
        "License set into local storage successfully " +
          JSON.stringify(dictionary)
      );
      var message = {
        action: "check-license",
        key: key
      };
      sendPortMessage(message)
        .then(function(response) {
          console.log(
            "checkLicense: license is valid! " + JSON.stringify(response)
          );
          resolve(response);
        })
        .catch(function(response) {
          console.log(
            "checkLicense: license is invalid " + JSON.stringify(response)
          );
          reject(response);
        });
    });
  });
}

function storeToStorage(key, value) {
  return new Promise(function(resolve, reject) {
    let dictionary = null;
    getDictionary().then(function(existingDictionary) {
      dictionary = existingDictionary;
    });

    if (!dictionary) {
      console.log("storeToStorage: dictionary is empty. Initializing!");
      dictionary = {};
    }

    let store = {};
    dictionary[key] = value;
    store[STORAGE_KEY_NAME] = dictionary;

    chrome.storage.sync.set(store, function() {
      console.log("Saved to storage: " + JSON.stringify(store));
      resolve(dictionary);
    });
  });
}

function getDictionary() {
  return new Promise(function(resolve, reject) {
    chrome.storage.sync.get([STORAGE_KEY_NAME], function(result) {
      let dictionary = result[STORAGE_KEY_NAME];
      console.log(
        "Dictionary in local storage is currently is " +
          JSON.stringify(dictionary)
      );
      if (!dictionary) {
        dictionary = {};
      }
      resolve(dictionary);
    });
  });
}

function retrieveLicenseKey() {
  getDictionary().then(function(dictionary) {
    let license = dictionary[LICENSE_KEY_NAME];
    if (license) {
      console.log("License key from local storage is " + license);
      $("#license-key").val(license);
    } else {
      console.log("License is not yet saved to storage");
    }
  });
}

function saveLicenseKey(value) {
  console.log("Saving license into storage", value);
  storeToStorage(LICENSE_KEY_NAME, value);
}
