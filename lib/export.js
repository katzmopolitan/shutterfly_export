ravenKey = "https://299729fd84784563aace56b46da2e5fe@sentry.io/120735";
Raven.config(ravenKey).install();

function ShutterflyBackupService(collectionKey, albumKey, site) {
  this.collectionKey = collectionKey;
  this.albumKey = albumKey;
  this.site = site;
  this.totalDownloaded = 0;
  this.uniqueNames = false;

  Notification.requestPermission();
}

ShutterflyBackupService.prototype.showError = function(message) {
  if (Notification.permission !== "granted") {
    Notification.requestPermission();
  } else {
    var notification = new Notification("Shutterfly Extension", {
      icon:
        "https://lh4.googleusercontent.com/a-ZDvs4XVG_oCn6phMCUaEuYQFfojupispiNRtBKQuCYCDx6dcy4k4VmEAp7vpx2RUiPN1KWeg=s52-h52-e365-rw",
      body: message
    });

    notification.onclick = function() {
      window.open(
        "https://shutterflyextension.freshdesk.com/support/tickets/new"
      );
    };
  }
};

ShutterflyBackupService.prototype.fixJSON = function(brokenJSON) {
  console.log("[fixJSON]");
  console.log(brokenJSON);
  var durable = durableJsonLint(brokenJSON);
  return durable.json;
};

ShutterflyBackupService.prototype.downloadCurrentPage = function(data, status) {
  try {
    console.log("[downloadCurrentPage]");
    var res = data.responseText;
    var hash;
    res = this.fixJSON(res);
    console.log("Parsing result");
    hash = jQuery.parseJSON(res);

    if (!hash) {
      console.log("Result is undefined");
      this.raiseError("Results cannot be parsed");
      return;
    }
    var result = hash["result"]["section"];

    if (result.nodeType === "shutterflyItem") {
      console.log("[downloadCurrentPage] Page with one picture");
      var pictureLinks = [];
      pictureLinks.push(this.constructDownloadLink(result));
      this.downloadPictures(pictureLinks);
      return;
    }
    var pictureLinks = this.extractPicturesLinks(result.items);
    this.downloadPictures(pictureLinks);

    var perPage = result.pageSize;
    var totalCount = result.count;
    var startIndex = result.startIndex - 1;
    var totalPages = Math.ceil(totalCount / perPage);
    var currentPageNumber =
      totalPages - Math.floor((totalCount - startIndex) / perPage);
    console.log("startIndex = " + startIndex);
    console.log("perPage = " + perPage);
    console.log("totalCount = " + totalCount);
    console.log("totalPages = " + totalPages);
    console.log("currentPageNumber = " + currentPageNumber);

    if (currentPageNumber != totalPages) {
      nextStartIndex = startIndex + perPage + 1;
      console.log(
        "Going to retrieve the next page starting at index " + nextStartIndex
      );
      this.downloadOnePage(nextStartIndex, this.site, this.page, this.nodeId);
    } else {
      console.log("last page reached");
      this.postDownload(this.totalDownloaded);
    }
  } catch (e) {
    this.raiseError(e);
  }
};

ShutterflyBackupService.prototype.postDownload = function(totalDownloaded) {
  console.log(
    "[downloadPictures] Total cummulative downloaded: " + totalDownloaded
  );
  // Read it using the storage API
  chrome.storage.sync.get(["totalDownloaded"], function(items) {
    var storedValue = items.totalDownloaded;
    if (storedValue) {
      console.log("Downloaded " + storedValue + " items last time");
    } else {
      chrome.storage.sync.set({ totalDownloaded: totalDownloaded }, function() {
        console.log("Settings saved");
      });
    }
  });
};

ShutterflyBackupService.prototype.extractPicturesLinks = function(arr) {
  console.log("[extractPicturesLinks]");
  var names = [];
  for (var i = 0; i < arr.length; i++) {
    var link = this.constructDownloadLink(arr[i], names.length);
    names.push(link);
  }
  return names;
};

/**
 * Construct URL that is used to download the given picture item
 */
ShutterflyBackupService.prototype.constructDownloadLink = function(
  pictureItem,
  counter
) {
  var shutterflyLink =
    "https://cmd.shutterfly.com/commands/async/downloadpicture?site=site&";
  shutterflyLink = shutterflyLink + "id=" + pictureItem.shutterflyId + "&";
  shutterflyLink = shutterflyLink + "collectionKey=" + this.collectionKey + "&";
  shutterflyLink = shutterflyLink + "albumKey=" + this.albumKey + "&";
  title = pictureItem.title;
  if (this.uniqueNames) {
    title = this.totalDownloaded + counter + "_" + title;
  }
  shutterflyLink = shutterflyLink + "title=" + title;
  return shutterflyLink;
};

/**
 * Download a picture at URL to disk
 */
ShutterflyBackupService.prototype.downloadPictures = function(picturesLinks) {
  console.log("[downloadPictures]");
  for (var i = 0; i < picturesLinks.length; i++) {
    if (this.debug == true) {
      console.log("[downloadPictures]" + picturesLinks[i]);
    } else {
      this.saveToDisk(picturesLinks[i]);
    }
  }
  this.totalDownloaded += picturesLinks.length;
};

//http://muaz-khan.blogspot.com/2012/10/save-files-on-disk-using-javascript-or.html
ShutterflyBackupService.prototype.saveToDisk = function(fileURL, fileName) {
  console.log("[saveToDisk]" + fileURL + " " + fileName);
  if (!window.ActiveXObject) {
    var save = document.createElement("a");
    save.href = fileURL;
    save.target = "_blank";
    save.download = fileName || "unknown";

    var evt = new MouseEvent("build");
    evt.initMouseEvent(
      "click",
      true,
      true,
      window,
      1,
      0,
      0,
      0,
      0,
      false,
      false,
      false,
      false,
      0,
      null
    );
    save.dispatchEvent(evt);

    (window.URL || window.webkitURL).revokeObjectURL(save.href);
  }
};

ShutterflyBackupService.prototype.parseAlbumProperties = function() {
  console.log("[parseAlbumProperties]");
  var albumUrl = this.albumUrl;

  console.log("[parseAlbumProperties]" + "albumUrl: " + albumUrl);
  if (
    (match = albumUrl.match(
      /(http.?:\/\/)?(.+)\.shutterfly.com\/pictures\/(\d+)/
    ))
  ) {
    console.log(albumUrl + "is a valid NEW url");
    this.site = match[2];
    this.nodeId = match[3];
    this.page = this.site + "/pictures";
  }
  if (
    (match = albumUrl.match(/(http.?:\/\/)?(.+)\.shutterfly.com\/(.*)\/(\d+)/))
  ) {
    console.log(albumUrl + "is a valid LEGACY nested url");
    this.site = match[2];
    this.page = this.site + "/" + match[3];
    this.nodeId = match[4];
  } else if (
    (match = albumUrl.match(/(http.?:\/\/)?(.+)\.shutterfly.com\/(\d+)\/?/))
  ) {
    console.log(albumUrl + "is a valid LEGACY url");
    this.site = match[2];
    this.nodeId = match[3];
    this.page = this.site;
  } else {
    throw "[parseAlbumProperties]" + albumUrl + " is invalid";
  }

  Raven.setUserContext({
    site: this.site,
    nodeId: this.nodeId,
    page: this.page
  });

  if (typeof this.site === "undefined") {
    throw "site is not found";
  } else if (typeof this.nodeId === "undefined") {
    throw "nodeId is not found";
  }

  console.log("site = " + this.site);
  console.log("nodeId = " + this.nodeId);
};

ShutterflyBackupService.prototype.downloadOnePage = function(
  startIndex,
  site,
  page,
  nodeId
) {
  if (typeof site === "undefined") {
    console.log("Site is not provided");
    throw "Site is not provided";
  }
  chrome.extension.sendMessage(
    {
      action: "getitems",
      data: {
        site: site,
        startIndex: startIndex,
        page: page,
        nodeId: nodeId,
        format: "json"
      }
    },
    jQuery.proxy(this.downloadCurrentPage, this)
  );
};

ShutterflyBackupService.prototype.downloadAlbum = function() {
  console.log("downloadAlbum: Initated");
  var unsupportTypeMessage = "Album of this type is not supported.";
  var unsupportTypeError = new Error("Unsupported album");
  if (typeof this.collectionKey === "undefined" || !this.collectionKey) {
    this.raiseError(unsupportTypeError, { error: unsupportTypeMessage });
  }

  if (typeof this.albumKey === "undefined" || !this.albumKey) {
    this.raiseError(unsupportTypeErrore, { error: unsupportTypeMessage });
  }

  if (typeof this.site === "undefined" || !this.site) {
    console.log("site has not been provided");
    this.raiseError(unsupportTypeErrore, { error: unsupportTypeMessage });
  }

  try {
    var startIndex = 0;
    this.parseAlbumProperties();
    this.downloadOnePage(startIndex, this.site, this.page, this.nodeId);
  } catch (e) {
    this.raiseError(e);
  }
};

ShutterflyBackupService.prototype.raiseError = function(
  exception,
  options = {}
) {
  var errorMessage =
    "An error occurred while attempting to download pictures. We have been notified of this error. Click here to report your error.";
  var error = options["error"] || errorMessage;
  this.showError(error);
  Raven.captureException(exception);
  var stack = exception.stack;
  console.error(stack);
};
