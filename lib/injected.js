/*
 * Send a message to the content script with details about the album
 */

if (typeof Shr !== "undefined" && typeof Shr.S !== "undefined") {
  window.postMessage(
    {
      type: "FROM_PAGE",
      collectionKey: Shr.S.collectionKey,
      albumKey: Shr.S.albumKey,
      site: Shr.S.siteName
    },
    "*"
  );
} else {
  window.postMessage(
    {
      type: "FROM_PAGE",
      collectionKey: null,
      albumKey: null,
      site: null
    },
    "*"
  );
}
